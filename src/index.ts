import './environment';
import config from './config';
import app from './Server';
import logger from './shared/Logger';
import getNewToken from './GetTokenCommand';

// Start the server or run the token command
if (config.command === 'token') {
  getNewToken();
} else {
  app.listen(config.port, () => {
    logger.info('Express server started: http://localhost:' + config.port);
  });
}
