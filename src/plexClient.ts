import PlexAPI from 'plex-api';
import config from './config';
import plexPinAuth from './plexPinAuth';

export const PLEX_API_OPTIONS = {
  identifier: 'af17ef5a-40db-44c1-aa18-0b1235ba2bc0',
  product: 'Plex Exporter (Node)',
  version: config.package.version,
  // deviceName: process.env.COMPUTERNAME, // TODO: IS THIS AVAILABLE ON LINUX?
};

export const client = new PlexAPI({
  hostname: config.plexServer,
  token: config.token,
  username: config.username,
  password: config.password,
  options: PLEX_API_OPTIONS,
});
