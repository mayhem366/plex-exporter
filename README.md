## plex-exporter

![npm](https://img.shields.io/npm/v/plex-exporter?style=for-the-badge)
![npm downloads](https://img.shields.io/npm/dt/plex-exporter?style=for-the-badge)

A Prometheus exporter that exports metrics on Plex Media Server.

## Install

```sh
npm install plex-exporter
```

## Usage

```text
NAME:
   plex-exporter - A Prometheus exporter that exports metrics on Plex Media Server.

USAGE:
   plex-exporter command [arguments...]

COMMANDS:
   token  Get authentication token from plex.tv
   server Run the exporter server

GLOBAL OPTIONS:
   --config.path value, -c value     Path to config file (default: "/etc/prometheus/plex.yml")
   --port, -p value                  Port for server (default: 9594)
   --log-level value                 Verbosity level of logs (default: "info")
   --plex-server value, -s value     Address of Plex Media Server (e.g. 192.168.0.2)
   --token value, -t value           Authentication token for Plex Media Server if using token authentication
   --username value                  Username of the plex account if logging in using credentials
   --password value                  Password of the plex account if logging in using credentials
```

## Metrics

```text
# HELP plex_library_section_size_count Number of items within a library section
# TYPE plex_library_section_size_count gauge
plex_library_section_size_count{name="Photos",type="album"} 580
plex_library_section_size_count{name="Photos",type="photo"} 13883
plex_library_section_size_count{name="TV programmes",type="show"} 98
plex_library_section_size_count{name="TV programmes",type="season"} 487
plex_library_section_size_count{name="TV programmes",type="episode"} 6743
plex_library_section_size_count{name="Films",type="movie"} 683
plex_library_section_size_count{name="Music",type="artist"} 350
plex_library_section_size_count{name="Music",type="album"} 990
plex_library_section_size_count{name="Music",type="track"} 9662

# HELP plex_sessions_active Now playing
# TYPE plex_sessions_active gauge
plex_sessions_active{type="movie",title="My Neighbor Totoro",progress="0.8730076653251979",player_product="Plex Web",player_title="Opera",player_platform="Opera",player_state="paused"} 2
plex_sessions_active{type="track",title="My Body Is a Cage",progress="0.08050103846339618",player_product="Plex for Windows",player_title="MY_HOSTNAME",player_platform="windows",player_state="playing",artist="Peter Gabriel",album="Scratch My Back"} 2
```

## Authentication

The application needs to be authenticated before it can be used and there are 2 options available: pin authentication or credential authentication.

### Credential Authentication

Simply provide the username and password in the configuration file or use the `--username` and `--password` command line arguments to provide your plex credentials.

### Token Authentication

Simply run `plex-exporter token` to start the PIN retrieval process.
Follow the instructions and save the retrieved token to put in the configuration file or pass as a command line parameter later.

## Configuration

Instead of passing command line arguments, a configuration file can be used instead.

Specify the `--config.file` command line argument to point to a _yaml_ or _json_ file that contains the following properties:

```yaml
port: 9594
logLevel: 'info'
plexServer: '192.168.0.2'

token: 'thetoken'
# or
username: 'MyUser'
password: 'MySuperSecretPassword'
```

## Prometheus

Simply add the server to the _prometheus.yml_ file:

```yaml
- job_name: 'plex'
  scrape_interval: 1h
  static_configs:
    - targets: ['localhost:9594']
```
